'use strict';

let router ;
let app ;
let wd = new WikiData() ;

$(document).ready ( function () {
    vue_components.toolname = 'projector' ;
    Vue.use(VueClazyLoad);
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
            vue_components.loadComponents ( ['tool-translate','tool-navbar','wd-link',//'lazyload','commons-thumbnail','snak','value-validator','typeahead-search','widar','wd-date',
                'vue_components/main-page.html',
                'vue_components/item-list.html',
/*                'vue_components/snak-editor.html',
                'vue_components/label-editor.html',
                'vue_components/claim-references-qualifiers.html',
                'vue_components/batch-navigator.html',
                'vue_components/tabernacle-cell.html',
                'vue_components/table-page.html',
*/
                ] ) ,
/*
            new Promise(function(resolve, reject) {
                $.get ( './config.json' , function ( d ) {
                    wd.api = d.wd.api ;
                    wd.sparql_url = d.wd.sparql ;
                    resolve() ;
                } , 'json' ) ;
            } )
*/
    ] ) .then ( () => {
        wd_link_wd = wd ;

        const routes = [
            { path: '/', component: MainPage , props:true },
            { path: '/:project', component: MainPage , props:true },
//            { path: '/tab', component: TablePage , props:true },
//            { path: '/:topic', component: TablePage , props:true },
//            { path: '/tab/:mode/:main_init/:cols_init', component: TablePage , props:true },
        ] ;
        router = new VueRouter({routes}) ;
        app = new Vue ( { router } ) .$mount('#app') ;
    } ) ;
} ) ;
